resource "aws_instance" "app-project-devops1" {
  ami             = "ami-0c7217cdde317cfec"
  instance_type   = "t2.micro"
  key_name        = "terraform"
  security_groups = ["allow_ssh", "allow_http", "allow_egress"]
  user_data       = file("script.sh")

  tags = {
    Name = "application-prod"
  }
}
